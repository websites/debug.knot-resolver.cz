#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import fcntl
import logging
import os
import select
import subprocess
import time
import urllib.parse

import dns.name
import dns.rdatatype

def parse_params(environ):
    if environ.get('REQUEST_METHOD') != 'GET':
        raise ValueError('405', b'only HTTP GET is supported')
    params = urllib.parse.parse_qs(environ.get('QUERY_STRING', ''))
    logging.debug('got params: %s', params)
    mand_params = {'qname', 'qtype'}
    if frozenset(params.keys()) != mand_params:
        raise ValueError(
            '400',
            bytes('two mandatory parameters are {}'.format(mand_params),
                    encoding='ascii'))
    if len(params['qname']) != 1 or len(params['qtype']) != 1:
        raise ValueError(
            '400',
            bytes('parameters {} must have exactly one value'.format(mand_params),
                encoding='ascii'))

    # validate qname
    try:
        dns.name.from_text(params['qname'][0])
    except Exception as ex:
        raise ValueError('400', b'qname does not look like a valid DNS name')

    # validate qtype
    try:
        typenum = dns.rdatatype.from_text(params['qtype'][0])
    except Exception as ex:
        raise ValueError('400', b'qtype does not look like a supported type, use TYPE12345 notation')

    return params['qname'][0], 'TYPE{}'.format(typenum)

def read_output(process, deadline):
    fd = process.stdout.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

    while time.monotonic() < deadline and process.poll() is None:
        timeout = deadline - time.monotonic()
        rlist, _, _ = select.select([process.stdout], [], [], timeout)

        if not rlist:  # timeout
            break
        buf = rlist[0].read()
        if not buf:  # end of file
            # race between stdout closure and process termination (below)
            process.wait(timeout=1)
            break
        yield buf

    if process.poll() is None:
        logging.error('subprocess stdout read timeout, now %f, deadline %f, '
                'sending SIGTERM to %d',
                time.monotonic(), deadline, process.pid)
        process.terminate()
        try:
            output = process.communicate(timeout=1)
            yield output
        except subprocess.TimeoutExpired:
            logging.error('SIGTERM timeout, sending SIGKILL to %d', process.pid)
            process.kill()
            output = process.communicate(timeout=1)
            yield output
        yield b'\n\n[timeout; output is incomplete; killing container]'
    else:
        logging.debug('process return code: %s', process.poll())
        yield process.stdout.read()  # read leftovers
    # TODO: run docker rm?

def needs_update(filename, maxage):
    """return True if filename is older than maxage seconds"""
    try:
        lockstats = os.stat(filename)
        lasttry = lockstats.st_mtime
    except FileNotFoundError:
        # create empty file
        with open(filename, 'w'):
            pass
        lasttry = 0
    os.utime(filename)  # touch the file's mtime

    now = time.time()
    return now - lasttry > maxage

def run_query(qname, qtype, timeout=20):
    image_name = 'cznic/knot-resolver:latest'
    # try to update image every 10 minutes
    if needs_update('/tmp/query-image.lock', 600):
        logging.debug('attempting to update Docker image')
        subprocess.run(['docker', 'pull', image_name],
                       check=True, timeout=10)
    deadline = time.monotonic() + timeout
    logging.debug('subprocess run, now %f, deadline %f', time.monotonic(), deadline)
    try:
        docker = subprocess.Popen(['/usr/bin/docker',
            'run',
            '--network', 'host',
            '--rm',
            '--env', 'QNAME',
            '--env', 'QTYPE',
            image_name,
            '-n', '-c', '/etc/knot-resolver/kresd.conf'],
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
            env={
                'QNAME': qname,
                'QTYPE': qtype,
                },
            )
    except Exception as ex:
        return '500', b'failed to start container, please e-mail knot-resolver@labs.nic.cz'
    else:
        status = '200 Container started'
    return status, read_output(docker, deadline)

def application(environ, start_response):
    logging.basicConfig(level=logging.DEBUG)
    headers = [
                ('Content-type', 'text/plain; charset=utf-8'),
                ('Cache-Control', 'no-cache, must-revalidate')
            ]  # HTTP Headers

    try:
        qname, qtype = parse_params(environ)
        status, output = run_query(qname, qtype)
    except ValueError as ex:
        status, output = ex.args
        output = [output]

    start_response(status, headers)
    return output

# debug server
if __name__ == '__main__':
    import wsgiref.simple_server
    with wsgiref.simple_server.make_server('', 8000, application) as httpd:
        print("Serving HTTP on port 8000...")

        # Respond to requests until process is killed
        httpd.serve_forever()
